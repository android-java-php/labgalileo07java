/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.Galileo.controlpanel;

import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import org.Galileo.info.panelinfo1;
import org.Galileo.info.panelproblema;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */
public class LabGl07 extends javax.swing.JFrame {

    /**
     * Creates new form LabGl07
     */
    public LabGl07() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

   
private void accion(){
String a,b,c,d;
a= txnm1.getText();
b= txnm2.getText();
c= txnm3.getText();
d= txnm4.getText();
int a1, b1,c1,d1;
a1= Integer.parseInt(a);
b1= Integer.parseInt(b);
c1= Integer.parseInt(c);
d1= Integer.parseInt(d);
        if(a1>b1 && a1>c1 && a1>d1){
        txrespuesta.setText("El Numero Mayor es:"+a1);
        }else if(b1>a1 && b1>c1 && b1>d1){
            txrespuesta.setText("El Numero Mayor es:"+b1);
        
        }else if(c1>a1 && c1>b1 && c1>d1){
            txrespuesta.setText("El Numero Mayor es:"+c1);
        
        }else if(d1>a1 && d1>b1 && d1>c1){
            txrespuesta.setText("El Numero Mayor es:"+d1);
        
        }else if(a1==b1 && a1>c1 && a1>d1){
            txrespuesta.setText("El Numero:"+a1+"Es igual al numero: "+b1+"pero mayor que el numero:"+c1+"y el numero:"+d1);
        
        }else if(a1==c1 && a1>b1 && a1>d1){
            txrespuesta.setText("El Numero:"+a1+"Es igual al numero: "+c1+"pero mayor que el numero:"+b1+"y el numero:"+d1);
        
        }else if(a1==d1 && a1>c1 && a1>b1){
            txrespuesta.setText("El Numero:"+a1+"Es igual al numero: "+d1+"pero mayor que el numero:"+c1+"y el numero:"+b1);
        
        }else if(b1==c1 && b1>a1 && a1>d1){
            txrespuesta.setText("El Numero:"+b1+"Es igual al numero: "+c1+"pero mayor que el numero:"+a1+"y el numero:"+d1);
        
        }else if(b1==d1 && b1>a1 && b1>c1){
            txrespuesta.setText("El Numero:"+b1+"Es igual al numero: "+d1+"pero mayor que el numero:"+a1+"y el numero:"+c1);
        
        }else if(c1==d1 && c1>a1 && c1>b1){
            txrespuesta.setText("El Numero:"+c1+"Es igual al numero: "+d1+"pero mayor que el numero:"+a1+"y el numero:"+b1);
        
        }
        
        
        
    }
    
private void cancelar(){
txnm1.setText("");
txnm2.setText("");
txnm3.setText("");
txnm4.setText("");
txrespuesta.setText("");
}
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txnm1 = new javax.swing.JTextField();
        txnm2 = new javax.swing.JTextField();
        txnm4 = new javax.swing.JTextField();
        txnm3 = new javax.swing.JTextField();
        txrespuesta = new javax.swing.JTextField();
        btaceptar = new javax.swing.JButton();
        btcancelar = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        txnm1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txnm1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txnm1KeyTyped(evt);
            }
        });

        txnm2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txnm2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txnm2KeyTyped(evt);
            }
        });

        txnm4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txnm4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txnm4KeyTyped(evt);
            }
        });

        txnm3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txnm3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txnm3KeyTyped(evt);
            }
        });

        txrespuesta.setEditable(false);
        txrespuesta.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btaceptar.setText("Aceptar");
        btaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btaceptarActionPerformed(evt);
            }
        });

        btcancelar.setText("Cancelar");
        btcancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btcancelarActionPerformed(evt);
            }
        });

        jMenu1.setText("Salir");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu1MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Info");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu2MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        jMenu3.setText("¿?");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu3MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(35, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txnm3, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(txnm4, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txnm1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(txnm2, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txrespuesta)
                    .addComponent(btaceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btcancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(31, 31, 31))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txnm1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txnm2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txnm3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txnm4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(txrespuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(btaceptar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btcancelar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MousePressed
System.exit(0);    // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1MousePressed

    private void btaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btaceptarActionPerformed
accion();      // TODO add your handling code here:
    }//GEN-LAST:event_btaceptarActionPerformed

    private void btcancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btcancelarActionPerformed
cancelar();      // TODO add your handling code here:
    }//GEN-LAST:event_btcancelarActionPerformed

    private void txnm1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txnm1KeyTyped
   int k = (int) evt.getKeyChar();
        if (k > 58 && k < 255) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }   
        
        
     
        
        
        
        
            int d = (int) evt.getKeyChar();
        if (d > 32 && d < 47) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }        // TODO add your handling code here:
    }//GEN-LAST:event_txnm1KeyTyped

    private void txnm2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txnm2KeyTyped
    int k = (int) evt.getKeyChar();
        if (k > 58 && k < 255) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }   
        
        
     
        
        
        
        
            int d = (int) evt.getKeyChar();
        if (d > 32 && d < 47) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }       // TODO add your handling code here:
    }//GEN-LAST:event_txnm2KeyTyped

    private void txnm3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txnm3KeyTyped
  int k = (int) evt.getKeyChar();
        if (k > 58 && k < 255) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }   
        
        
     
        
        
        
        
            int d = (int) evt.getKeyChar();
        if (d > 32 && d < 47) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }         // TODO add your handling code here:
    }//GEN-LAST:event_txnm3KeyTyped

    private void txnm4KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txnm4KeyTyped
   int k = (int) evt.getKeyChar();
        if (k > 58 && k < 255) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }   
        
        
     
        
        
        
        
            int d = (int) evt.getKeyChar();
        if (d > 32 && d < 47) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }        // TODO add your handling code here:
    }//GEN-LAST:event_txnm4KeyTyped

    private void jMenu2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MousePressed
    new panelinfo1().setVisible(true);    // TODO add your handling code here:
    }//GEN-LAST:event_jMenu2MousePressed

    private void jMenu3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MousePressed
        new panelproblema().setVisible(true);    // TODO add your handling code here:
    }//GEN-LAST:event_jMenu3MousePressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LabGl07.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LabGl07.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LabGl07.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LabGl07.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LabGl07().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btaceptar;
    private javax.swing.JButton btcancelar;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JTextField txnm1;
    private javax.swing.JTextField txnm2;
    private javax.swing.JTextField txnm3;
    private javax.swing.JTextField txnm4;
    private javax.swing.JTextField txrespuesta;
    // End of variables declaration//GEN-END:variables
}
